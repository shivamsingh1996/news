<style type="text/css">
    .remove-me
    {
        margin-bottom: 2px;
    }
    .input-remove
    {
        margin-bottom: 2px;
    }
</style>
<div class="tab-content">
    <?php
    if(isset($msg))
    {
        echo $msg;
    }
    if(isset($img_error))
    {
        foreach ($img_error as $error)
        {
            echo '<span style="color: red;font-size: 18px;">'.$error.'</span>';
        }
    }
    ?>
    <div class="tab-pane active" id="tab_0">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Add News
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
            </div>

            <div class="portlet-body form">

                <!-- BEGIN FORM-->
                <form action="<?php echo base_url('admin/add_image/').$upload_id; ?>" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Select no of image</label>
                            <div class="col-md-6">
                                <div id="field">
                                    <div>
                                        <input class="input col-md-7" id="field1" name="userfile[]" type="file" />
                                        <button id="b1" class="btn add-more col-md-1" type="button">+</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inner">

                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <!--<input type="submit"  value="submit" class="btn btn-circle blue">-->
                                    <input type="submit" name="submit" value="submit" class="btn btn-circle blue">
                                    <button type="button" class="btn btn-circle default">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>


            </div>
        </div>
    </div>

    <?php
    if(isset($return)) {
        $x = $return->result();
        $count = count($x);
        if ($count > 0) {
            $count_img = $x[0]->image_count;
            if ($count_img > 0) {
                $y = explode(',', $x[0]->image);

                if (count($y) > 1) {
                    foreach ($y as $value) {
                        ?>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_0">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Add News
                                        </div>
                                    </div>

                                    <div class="portlet-body form">
                                        <img src="<?= base_url() ?>uploads/add/<?= $value ?>"
                                             class="img-responsive center-block" style="padding: 50px; width: 50%">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                } elseif (count($y) == 1) {
                    ?>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_0">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>Add News
                                    </div>
                                </div>

                                <div class="portlet-body form">
                                    <img src="<?= base_url() ?>uploads/add/<?= $x[0]->image ?>"
                                         class="img-responsive center-block" style="padding: 50px; width: 50%">
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                } else {
                    echo '';
                }
            }
        }
    }
    ?>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var next = 1;
        $(".add-more").click(function(e){
            e.preventDefault();
            var addto = "#field" + next;
            var addRemove = "#field" + (next);
            next = next + 1;
            var newIn = '<input class="input col-md-7 input-remove" id="field' + next + '" name="userfile[]" type="file">';
            var newInput = $(newIn);
            var removeBtn = '<button id="remove' + (next - 1) + '" class="btn btn-danger remove-me col-md-1" >-</button></div><div id="field">';
            var removeButton = $(removeBtn);
            $(addto).after(newInput);
            $(addRemove).after(removeButton);
            $("#field" + next).attr('data-source',$(addto).attr('data-source'));
            $("#count").val(next);

            $('.remove-me').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#field" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
        });



    });

</script>

