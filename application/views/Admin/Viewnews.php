<a href="<?php echo base_url('Admin/addnews') ?>" class="btn btn-success"><i class="icon-plus"></i> Add News</a>
<br/><br/>
<div class="portlet box blue-hoki table-responsive">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Customer
							</div>
							<div class="tools">
							</div>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
							<tr>
								<td>Sr.
								</td>
								<th>
									Title
								</th>
								<th>
									 Source Link
								</th>
								<th>
									 Category
								</th>
								
								<th>
									Action
								</th>
								
							</tr>
							</thead>
							<tbody>
								<?php
				$n = 0;
				foreach($blogs->result() as $blog){
					$n++;
				?>
							<tr>
								<td>
									<?=$n;?>
								</td>
								<td>
									<?=$blog->title; ?>
								</td>
								<td>
									<?=$blog->source_link; ?>
								</td>
								<td>
									<?=$blog->category;?>
								</td>
								
								<td>
								  <span>
                                    <a href="<?=base_url('admin/editnews/'.$blog->id) ?>" class="btn btn-info"><i class="icon-note"></i> </a>
                                  </span>
                                 <span>
									<a href="<?=base_url('admin/delete/'.$blog->id) ?>" class="btn btn-danger" onclick="return confirm('Do you want to delete this record');"><i class="icon-close"></i></a>
                                 </span>
                                </td>
                            </tr>

								<?php
			}

				?>
                            </tbody>
							</table>
						</div>
					</div>
					<script>
jQuery(document).ready(function() {       
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
QuickSidebar.init(); // init quick sidebar
Demo.init(); // init demo features
   TableAdvanced.init();
});
</script>