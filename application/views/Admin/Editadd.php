
<?php
$update_id = $this->uri->segment(3);

if(isset($update_id))
{
    echo '<a href="'.base_url('admin/add_image/').$update_id.'" class="btn btn-success"><i class="icon-plus"></i> Add image</a><br/><br/>';
}
?>
<div class="tab-content">
							<div class="tab-pane active" id="tab_0">
								<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i>Edit News
										</div>
										<div class="tools">
											<a href="javascript:;" class="collapse">
											</a>
											<a href="#portlet-config" data-toggle="modal" class="config">
											</a>
											<a href="javascript:;" class="reload">
											</a>
											<a href="javascript:;" class="remove">
											</a>
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										
										<form action="<?php echo base_url('admin/updateadd'); ?> " class="form-horizontal" method="post">
											<div class="form-body">
												<input type="hidden" name="txt_hidden" value="<?php echo $blog->id;?>">
													
												<div class="form-group">
													<label class="col-md-3 control-label">Source Link</label>
													<div class="col-md-6">
														<input type="text" class="form-control input-circle" placeholder="First Name" name="source_link" value="<?php echo $blog->link; ?>">
														
													</div>
												</div>
												<div class="form-group">
      											<label class="col-md-3 control-label">Choose The Category</label>
      											<div class="col-md-6">
      											<select class="form-control input-circle" id="sel1" name="category">
      												<?php
      												if($cat){
															$n = 0;
														foreach($cat as $cate){
															$n++
														?>
        										<option value="<?php echo $cate->id;?>"> <?php echo $cate->category; ?></option>
        												<?php
														}
													}
												?>
      											</select>
      											

      											</div>
      											
      											</div>


      											
												
      											
				
													<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<!--<input type="submit"  value="submit" class="btn btn-circle blue">-->
														<input type="submit" name="submit" value="update" class="btn btn-circle blue">
														<a href="<?php echo base_url('admin/newslist');?>" class="btn btn-circle default">Back</a>
													</div>
												</div>
											</div>
										</form>
										
										
									</div>
								</div>
							</div>
						</div>
					</div>
