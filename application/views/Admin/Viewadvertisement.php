<a href="<?php echo base_url('Admin/addadvertisement');?>" class="btn btn-success">Add Advertisement</a>
<br/><br/>
<div class="portlet box blue-hoki table-responsive">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>View  Advertisement
							</div>
							<div class="tools">
							</div>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
							<tr>
								<td>Sr.
								</td>
								
								<th>
									 Source Link
								</th>
								<th>
									 Category
								</th>
								
								<th>
									Action
								</th>
								
							</tr>
							</thead>
							<tbody>
								<?php
                            if($blogs){
                                $n = 0;
                            foreach($blogs->result() as $blog){
                                $n++;
                            ?>
							<tr>
								<td>
									<?php echo $n;?>
								</td>
								
								<td>
									<?php echo $blog->link; ?>
								</td>
								<td>
									<?php echo $blog->category; ?>
								</td>
								
								<td>
									<a href="<?php echo base_url('admin/editadd/'.$blog->id) ?>" class="btn btn-info">Edit</a>
									<a href="<?php echo base_url('admin/deleteadd/'.$blog->id) ?>" class="btn btn-danger" onclick="return confirm('Do you want to delete this record');">Delete</a>
								</td>
								
								
							</tbody>
								<?php
                            }
                        }
                                ?>
							</table>
						</div>
					</div>
					<script>
jQuery(document).ready(function() {       
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
QuickSidebar.init(); // init quick sidebar
Demo.init(); // init demo features
   TableAdvanced.init();
});
</script>