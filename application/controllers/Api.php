<?php
/**
 * Created by PhpStorm.
 * User: shivam
 * Date: 1/11/17
 * Time: 6:53 PM
 */
class Api extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
//get news using category name
    function create_news()
    {
       $category_id = $this->uri->segment(3);
       if(empty($category_id))
       {
           echo 'Please enter category name';
       }
       if(!is_numeric($category_id))
       {
           echo 'Category id is not numeric';
       }
       $query = "SELECT * from category
               INNER JOIN news
               ON category.id = news.category_id
               WHERE category.id = $category_id";
       $return = $this->_custom_query($query);
       echo stripslashes(json_encode($return->result()));
    }
//model connect function
//display news
    function display_news()
    {
        $news_id = $this->uri->segment(3);
        if(empty($news_id))
        {
            echo 'Please enter News id';
        }
        if(!is_numeric($news_id))
        {
            echo 'news id is not numeric';
        }
        $query = "select * from news where id = $news_id";
        $return = $this->_custom_query($query);
        echo stripslashes(json_encode($return->result()));
    }
//recomendation news
    function recomendations()
    {
        $x= json_decode($_POST['x'],true);
        $y=array();
        foreach($x as $key=>$value)
        {
            foreach($value as $k=>$v)
            {
                if($k=='label')
                {
                    $news =  $this->_custom_query("select * from news where category='$v' order by id desc  LIMIT 5");
                    if(!empty($news))
                    {
                        $y[] = $news;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
        }

        $a = json_encode($y, JSON_UNESCAPED_SLASHES);
        if($a == '[]')
        {
            echo '[]';
        }
        else
        {

            echo substr(substr((str_replace('],[', ',', $a)),1),0,-1);

        }
    }
//google interests

    function google_interests()
    {
        $google_id = $this->input->post('google_id',true);
        $interests = $this->input->post('interests',true);


        $return = $this->_custom_query("select * from user where google_id = '$google_id'");
        if(empty($return->result()))
        {
            $this->_custom_query("insert into user (google_id,interest) values ('$google_id','$interests')");
        }
        else
        {
            $this->_custom_query("update user set interest='$interests' where google_id = '$google_id'");
        }

        echo $interests;
    }
    function advertisement()
    {
        $add_id = $this->uri->segment(3);

        if(!is_numeric($add_id))
        {
            echo 'please enter advertisement id';
        }

        $query = "";
    }
    function _get($order_by)
    {
        $this->load->model('Api_model');
        return $this->Api_model->get($order_by);
    }
    function _insert($data)
    {
        $this->load->model('Api_model');
        $this->Api_model->_insert($data);
    }
    function _get_where($update_id)
    {
        $this->load->model('Api_model');
        $return = $this->Api_model->get_where($update_id);
        return $return;
    }
    function _update($id, $data)
    {
        if (!is_numeric($id)) {
            die('Non-numeric variable!');
        }

        $this->load->model('Api_model');
        $this->Api_model->_update($id, $data);
    }
    function _custom_query($query)
    {
        $this->load->model('Api_model');
        return $this->Api_model->_custom_query($query);
    }
}