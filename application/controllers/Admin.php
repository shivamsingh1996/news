<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('Admin_m');
	}
	public function submit(){
		$result=$this->Admin_m->submit();
		if($result){
			$this->session->set_flashdata('success_msg','Add data successfully');

		}
		else
		{
			$this->session->set_flashdata('error_msg','Unable To Connect');

		}
		redirect(base_url('Admin/index'));
	}

	
	public function index()
	{
		
		$this->load->view('Admin/Login_soft');
		
	}
	public function process(){
		 $username = $this->security->xss_clean($this->input->post('username'));
         $password = $this->security->xss_clean($this->input->post('password'));

        
        $this->load->model('Admin_m');
       
        $result = $this->Admin_m->validate($username,$password);
        // Now we verify the result
        if($result === FALSE){

            // If user did not validate, then show them login page again
            $this->index();
        }else{
            // If user did validate, 
            // Send them to members area
            redirect('Admin/dashboard');
        }        
}
	public function dashboard(){
	    $this->load->view('Admin/Header');
		$this->load->view('Admin/Dashboard');
		$this->load->view('Admin/Footer');
	}
	
	public function addnews()
	{

		$this->load->helper(array('form','url'));
		$this->load->helper('email');
		$this->load->library('form_validation');
		$data['cat'] = $this->Admin_m->fetch_category();
		
		if($this->input->post('submit') == 'submit'){
			$this->form_validation->set_rules('title','title','trim|required');
			$this->form_validation->set_rules('source_link','Source link','trim|required');
			$this->form_validation->set_rules('category','Category','trim|required');
			$this->form_validation->set_rules('content','Content','trim|required');
			
			if($this->form_validation->run() == FALSE){
                $this->load->view('Admin/Header');
				$this->load->view('Admin/Addnews');
                $this->load->view('Admin/Footer');
			}
			else{
				
			$result=$this->Admin_m->addnews();
		if($result){
			$this->session->set_flashdata('success_msg','News data successfully');

		}
		else
		{
			$this->session->set_flashdata('error_msg','Unable To Add');

		}
		redirect(base_url('Admin/dashboard'));
	}
		
	}
			else{
                $this->load->view('Admin/Header');
				$this->load->view('Admin/Addnews',$data);
                $this->load->view('Admin/Footer');
			}
		
	}
	public function addadvertisement()
	{


		$this->load->helper(array('form','url'));
		$this->load->helper('email');
		$this->load->library('form_validation');
		$data['cat'] = $this->Admin_m->fetch_category();
		
		if($this->input->post('submit') == 'submit'){
			
			$this->form_validation->set_rules('source_link','Source link','trim|required');
			$this->form_validation->set_rules('category','Category','trim|required');
			
			
			if($this->form_validation->run() == FALSE){

                $this->load->view('Admin/Header');
				$this->load->view('Admin/Addadvertisement');
                $this->load->view('Admin/Footer');
				
			}
			else{
				
			$result=$this->Admin_m->addadvertisement();
		if($result){
			$this->session->set_flashdata('success_msg','Advertisement add successfully');

		}
		else
		{
			$this->session->set_flashdata('error_msg','Unable To Add');

		}
		redirect(base_url('Admin/dashboard'));
	}
		
	}
			else{
                $this->load->view('Admin/Header');
				$this->load->view('Admin/Addadvertisement',$data);
                $this->load->view('Admin/Footer');
			}
		
	}
	public function logout(){

            $this->session->sess_destroy();
        	redirect('admin/index');
    }
    public function newslist(){

    	$data['blogs'] = $this->Admin_m->_custom_query("select news.id,category.category,news.title,news.source_link from news 
                                INNER  JOIN  category 
                                on category.id = news.category_id");
        $this->load->view('Admin/Header',$data);
    	$this->load->view('Admin/Viewnews');
        $this->load->view('Admin/Footer');
    }
   public function viewadd(){

    	$data['blogs'] = $this->Admin_m->_custom_query("select category.category,ads.link,ads.id from ads 
                                                    INNER  JOIN  category 
                                                    on category.id = ads.category_id");
        $this->load->view('Admin/Header');
    	$this->load->view('Admin/Viewadvertisement',$data);
        $this->load->view('Admin/Footer');
    }
    public function delete($id){
		$result= $this->Admin_m->delete($id);
		if($result){
			$this->session->set_flashdata('success_msg','Record Delete successfullt');

		}
		else
		{
			$this->session->set_flashdata('error_msg','Failed to Delete record');

		}
		redirect(base_url('Admin/dashboard'));


	}
	public function deleteadd($id){
		$result= $this->Admin_m->deleteadd($id);
		if($result){
			$this->session->set_flashdata('success_msg','Record Delete successfullt');

		}
		else
		{
			$this->session->set_flashdata('error_msg','Failed to Delete record');

		}
		redirect(base_url('Admin/dashboard'));


	}
	public function editnews($id)
    {	$data['cat'] = $this->Admin_m->fetch_category();
    	$data['blog'] = $this->Admin_m->getBlogByID($id);

        $this->load->view('Admin/Header');
    	$this->load->view('Admin/Editnews',$data);
        $this->load->view('Admin/Footer');
    }
    public function editadd($id)
    {
    	$data['blog'] = $this->Admin_m->getBlogid($id);
    	$data['cat'] = $this->Admin_m->fetch_category();

        $this->load->view('Admin/Header');
    	$this->load->view('Admin/Editadd',$data);
        $this->load->view('Admin/Footer');
    }
    public function update(){
		$result = $this->Admin_m->update();
		if($result){
			$this->session->set_flashdata('success_msg','Record updated successfullt');

		}
		else
		{
			$this->session->set_flashdata('error_msg','Failed to update record');

		}
		redirect(base_url('Admin/dashboard'));
		
	}
	public function updateadd(){
		$result = $this->Admin_m->updateadd();
		if($result){
			$this->session->set_flashdata('success_msg',' Advertisement updated successfullt');

		}
		else
		{
			$this->session->set_flashdata('error_msg','Failed to update record');

		}
		redirect(base_url('Admin/dashboard'));
		
	}
	function edit_news_upload_image()
    {
        $id = $this->uri->segment(3);
        if(!is_numeric($id))
        {
            redirect(base_url('admin/newslist'));
        }
        $this->load->model('Admin_m');
        $data['return'] = $this->Admin_m->_custom_query("select * from news where id =$id");
        $data['upload_id'] = $id;
        $data['img_error'] = $this->session->flashdata('img_error');
        $data['msg'] = $this->session->flashdata('item');
        $this->load->view('Admin/Header',$data);
        $this->load->view('Admin/Upload_news_image');
        $this->load->view('Admin/Footer');
    }
    function upload_image()
    {
        $upload_id = $this->uri->segment(3);
        if(!is_numeric($upload_id))
        {
            redirect(base_url('admin/newslist'));
        }

        $this->load->library('upload');
        $image_count = $this->input->post('image_count');
        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);
        $new_name = '';
        for($i=0; $i<$cpt; $i++)
        {
            $_FILES['userfile']['name']= $files['userfile']['name'][$i];
            $_FILES['userfile']['type']= $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error']= $files['userfile']['error'][$i];
            $_FILES['userfile']['size']= $files['userfile']['size'][$i];

            $config = array();
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = './uploads/news/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']             = 1000;
            $config['max_width']            = 3024;
            $config['max_height']           = 3024;


            $this->upload->initialize($config);

            if (!$this->upload->do_upload())
            {
                $error = array('error' => $this->upload->display_errors());

                $this->session->set_flashdata('img_error',$error);
                redirect(base_url('admin/edit_news_upload_image/'.$upload_id));
            }
            else
            {
                $data = array('upload_data' => $this->upload->data());

                //$this->load->view('upload_success', $data);
                $data = array('upload_data' => $this->upload->data());
                $upload_data = $data['upload_data'];
                $file_name = $upload_data['file_name'];
                $new_name .= $file_name.',';
            }
        }
        $new_name = rtrim($new_name,',');
        $this->load->model('Admin_m');
        $this->Admin_m->_custom_query("update news set image='$new_name',image_count = $image_count where id =  $upload_id");
        $msg = "Image inserted into database";
        $value = '<div class="alert alert-success">' . $msg . '</div>';
        $this->session->set_flashdata('item', $value);
        redirect(base_url('admin/edit_news_upload_image/'.$upload_id));
    }
    function add_image()
    {
        $submit = $this->input->post('submit');
        $upload_id =  $this->uri->segment(3);

        if($submit == 'submit')
        {

            $this->load->library('upload');
            $files = $_FILES;
            $cpt = count($_FILES['userfile']['name']);
            $new_name = '';
            for($i=0; $i<$cpt; $i++)
            {
                $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                $_FILES['userfile']['size']= $files['userfile']['size'][$i];

                $config = array();
                $config['encrypt_name'] = TRUE;
                $config['upload_path'] = './uploads/add/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']             = 1000;
                $config['max_width']            = 3024;
                $config['max_height']           = 3024;


                $this->upload->initialize($config);
                unset($config);
                if (!$this->upload->do_upload())
                {
                    $error = array('error' => $this->upload->display_errors());

                    $this->session->set_flashdata('img_error',$error);
                    redirect(base_url('admin/add_image/'.$upload_id));
                }
                else
                {

                    //$this->load->view('upload_success', $data);
                    $data = array('upload_data' => $this->upload->data());
                    $upload_data = $data['upload_data'];
                    $file_name = $upload_data['file_name'];

                    $new_name .= $file_name.',';
                }
            }
            $new_name = rtrim($new_name,',');
            $this->load->model('Admin_m');
            $this->Admin_m->_custom_query("update ads set image='$new_name', image_count=$cpt where id =  $upload_id");
            $msg = "Image inserted into database";
            $value = '<div class="alert alert-success">' . $msg . '</div>';
            $this->session->set_flashdata('item', $value);
            redirect(base_url('admin/add_image/'.$upload_id));

        }
        if(is_numeric($upload_id))
        {

            $data['return'] = $this->Admin_m->_custom_query("select * from ads where id=$upload_id");
        }
        $data['img_error'] = $this->session->flashdata('img_error');
        $data['msg']=$this->session->flashdata('item');
        $data['upload_id'] =$upload_id;
        $this->load->view('Admin/Header',$data);
        $this->load->view('Admin/Add_image');
        $this->load->view('Admin/Footer');
    }

}

	